package com.example.task.repository;

import com.example.task.entity.Organization;
import com.example.task.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;

public interface OrganizationRepository extends JpaRepository<Organization,Long> {


}
