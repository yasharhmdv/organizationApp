package com.example.task.repository;

import com.example.task.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;

public interface UserRepository extends JpaRepository<Users,Long> {

    Boolean existsUsersByName(String name);
    Boolean existsByEmail(String email);

    @Query(value = "select u from Users u join fetch u.organization o where o.id = :id")
    Collection<Users> findAllByUsersId(Long id);
}
