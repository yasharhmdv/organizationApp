package com.example.task.repository;

import com.example.task.entity.UserTaskAssignment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserTaskAssignmentRepository extends JpaRepository<UserTaskAssignment,Long> {
}
