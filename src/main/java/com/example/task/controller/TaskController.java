package com.example.task.controller;

import com.example.task.dto.TaskRequest;
import com.example.task.dto.TaskResponse;
import com.example.task.service.TaskService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/task")
@RequiredArgsConstructor
public class TaskController {
    private final TaskService taskService;

    @PostMapping("/create")
    public ResponseEntity<String> create(@RequestBody TaskRequest request){
        taskService.create(request);
        return new  ResponseEntity<>("Task created successfully", HttpStatus.OK);
    }
    @GetMapping("/{taskId}")
    public TaskResponse get(@PathVariable Long taskId){
        return taskService.get(taskId);

    }
}
