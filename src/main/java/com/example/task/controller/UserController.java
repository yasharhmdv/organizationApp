package com.example.task.controller;

import com.example.task.dto.UserRequest;
import com.example.task.dto.UserResponse;
import com.example.task.entity.Organization;
import com.example.task.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
public class UserController {
    private final UserService userService;


    @PostMapping("/create")
    public ResponseEntity<String> create(@RequestBody UserRequest request){
        userService.create(request);
        return new ResponseEntity<>("User created successfully", HttpStatus.OK);
    }

    @GetMapping("/{userId}")
    public UserResponse get(@PathVariable Long userId){
        return userService.get(userId);

    }

    @PutMapping("/{userId}")
    public UserResponse update(@PathVariable Long userId, @RequestBody UserRequest request){
        return userService.update(userId,request);
    }

    @DeleteMapping("/{userId}")
    public void delete(@PathVariable Long userId){
        userService.delete(userId);
    }


}
