package com.example.task.controller;

import com.example.task.dto.OrganizationRequest;
import com.example.task.dto.OrganizationResponse;
import com.example.task.dto.UserResponse;
import com.example.task.service.OrganizationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/organization")
public class OrganizationController {
    private final OrganizationService organizationService;

    @PostMapping
    public void create(@RequestBody OrganizationRequest request){
        organizationService.create(request);
    }

    @GetMapping("/{organizarionId}")
    public OrganizationResponse get(@PathVariable Long organizarionId){
        return organizationService.get(organizarionId);
    }



}
