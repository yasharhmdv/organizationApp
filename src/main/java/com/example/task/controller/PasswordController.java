package com.example.task.controller;

import com.example.task.dto.PasswordDto;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/password")
public class PasswordController {

    @PostMapping
    public ResponseEntity<String> save(@RequestBody @Validated PasswordDto dto){
        return ResponseEntity.ok("Success");
    }
}
