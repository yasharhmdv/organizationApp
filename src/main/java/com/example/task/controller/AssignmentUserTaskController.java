package com.example.task.controller;

import com.example.task.entity.Task;
import com.example.task.entity.Users;
import com.example.task.service.AssignmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/assign")
public class AssignmentUserTaskController {

    private final AssignmentService assignmentService;

    @PostMapping("/assign-users/{userIds}")
    public ResponseEntity<Users> assignTasksToUser(@PathVariable Long userIds, @RequestBody List<Long> taskIds) {
        Users user = assignmentService.assignTasksToUser(userIds, taskIds);
        return ResponseEntity.ok(user);
    }
    @GetMapping("/tasks/{userId}")
    public ResponseEntity<List<Task>> getAssignedTasks(@PathVariable Long userId) {
        List<Task> tasks = assignmentService.getAssignedTasks(userId);
        return ResponseEntity.ok(tasks);
    }



    @PostMapping("/assign-tasks/{taskIds}")
    public ResponseEntity<Task> assignUsersToTask(@PathVariable Long taskIds, @RequestBody List<Long> userIds) {
        Task task = assignmentService.assignUsersToTask(taskIds, userIds);
        return ResponseEntity.ok(task);
    }
    }






















    /*public ResponseEntity<?> assignTasksToUsers(List<Long> taskIds, List<Long> userIds){

        try {
            for (Long taskId : taskIds){
                Optional<Task> taskOptional =taskRepository.findById(taskId);
                if (taskOptional != null){
                    for (Long userId : userIds){
                        Optional<Users> usersOptional = userRepository.findById(userId);
                        if (usersOptional != null){
                            UserTaskAssignment assignment = new UserTaskAssignment();
                            Task task = modelMapper.map(taskOptional,Task.class);
                            assignment.setTask(task);
                            Users users = modelMapper.map(usersOptional, Users.class);
                            assignment.setUsers(users);

                        }
                    }
                }
            }

        }
        catch (Exception e){
            e.printStackTrace();
            return new ResponseEntity<String>("Unsucces", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<String>("Succes", HttpStatus.OK);
    }*/

