package com.example.task.dto;

import com.example.task.entity.Users;
import lombok.*;

import java.util.Set;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrganizationResponse {
    private Long id;
    private String name;

    private Set<Users> users;

}
