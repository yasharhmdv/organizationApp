package com.example.task.dto;

import com.example.task.enums.Status;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TaskRequest {
    private Date deadline;
    private Status status;
    private String description;
}
