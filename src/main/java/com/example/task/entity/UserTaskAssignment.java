package com.example.task.entity;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table
public class UserTaskAssignment {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private Users users;

    @ManyToOne
    private Task task;
}
