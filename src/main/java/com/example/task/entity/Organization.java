package com.example.task.entity;

import com.example.task.dto.UserResponse;
import jakarta.persistence.*;
import lombok.*;

import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
/*@NamedEntityGraph(
        name = "organization_task",
        attributeNodes = {
                @NamedAttributeNode("user"),
                @NamedAttributeNode("organization")
      })*/

@Table(name = "organization")
public class Organization {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    @OneToMany(mappedBy = "organization", cascade = CascadeType.PERSIST)
    private Set<Users> users = new HashSet<>();

    @OneToMany(mappedBy = "organization", cascade = CascadeType.PERSIST)
    private Set<Task> tasks = new HashSet<>();

}

