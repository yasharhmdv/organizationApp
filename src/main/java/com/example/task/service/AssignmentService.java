package com.example.task.service;
import com.example.task.entity.Task;
import com.example.task.entity.UserTaskAssignment;
import com.example.task.entity.Users;
import com.example.task.repository.TaskRepository;
import com.example.task.repository.UserRepository;
import com.example.task.repository.UserTaskAssignmentRepository;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AssignmentService {
    private final TaskRepository taskRepository;
    private final UserRepository userRepository;
    private final UserTaskAssignmentRepository assignmentRepository;
   // private final UserTaskAssignment assignment;


    public Users assignTasksToUser(Long userIds, List<Long> taskIds) {
        Users user = userRepository.findById(userIds).orElseThrow(() ->
                new EntityNotFoundException("User not found"));
        List<UserTaskAssignment> assignments = new ArrayList<>();
        for (Long taskId : taskIds) {
            Task task = taskRepository.findById(taskId).orElseThrow(() ->
                    new EntityNotFoundException("Task not found"));
            UserTaskAssignment assignment = new UserTaskAssignment();
            assignment.setUsers(user);
            assignment.setTask(task);
            assignments.add(assignment);
            assignmentRepository.save(assignment);
        }
        user.setTaskAssignments(assignments);
        userRepository.save(user);
        return user;
    }

    public List<Task> getAssignedTasks(Long userId) {
        Users user = userRepository.findById(userId).orElseThrow(() ->
                new RuntimeException());
        List<UserTaskAssignment> assignments = user.getTaskAssignments();
        return assignments.stream().map(UserTaskAssignment::getTask).collect(Collectors.toList());
    }

    public Task assignUsersToTask(Long taskId, List<Long> userIds) {
        Task task = taskRepository.findById(taskId).orElseThrow(() ->
                new NullPointerException("Task not found"));
        List<UserTaskAssignment> assignments = new ArrayList<>();
        for (Long userId : userIds) {
            Users user = userRepository.findById(userId).orElseThrow(() ->
                    new NullPointerException("User not found"));
            UserTaskAssignment assignment = new UserTaskAssignment();
            assignment.setUsers(user);
            assignment.setTask(task);
            assignments.add(assignment);
        }
        task.setUserAssignments(assignments);
        taskRepository.save(task);
        return task;
    }
}

