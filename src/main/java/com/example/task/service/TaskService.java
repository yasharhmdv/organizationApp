package com.example.task.service;

import com.example.task.dto.TaskRequest;
import com.example.task.dto.TaskResponse;
import com.example.task.entity.Task;
import com.example.task.repository.TaskRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TaskService {
    private final TaskRepository taskRepository;
    private final ModelMapper modelMapper;
    public ResponseEntity<?> create(TaskRequest request) {

        Task task = modelMapper.map(request,Task.class);
        taskRepository.save(task);
        return new ResponseEntity<>("Task created successfully", HttpStatus.OK);

    }

    public TaskResponse get(Long taskId) {
        Task task = taskRepository.findById(taskId).
                orElseThrow(()->new RuntimeException(String.format("There is no in %s id task",taskId)));
        return modelMapper.map(task,TaskResponse.class);
    }

}
