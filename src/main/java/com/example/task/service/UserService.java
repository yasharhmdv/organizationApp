package com.example.task.service;

import com.example.task.dto.UserRequest;
import com.example.task.dto.UserResponse;
import com.example.task.entity.Users;
import com.example.task.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.apache.catalina.User;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final ModelMapper modelMapper;

    public ResponseEntity<?> create(UserRequest request) {
        if (userRepository.existsUsersByName(request.getName())) {
            return new ResponseEntity<>("The Name has already taken!", HttpStatus.BAD_REQUEST);
        }
        if (userRepository.existsByEmail(request.getEmail())) {
            return new ResponseEntity<>("This email has already taken!", HttpStatus.BAD_REQUEST);
        }
         else {
            Users users = modelMapper.map(request, Users.class);
            userRepository.save(users);
            return new ResponseEntity<>("Password is strong", HttpStatus.OK);
        }
    }
    public UserResponse get(Long userId) {

        Users users = userRepository.findById(userId).
                orElseThrow(()->
                        new RuntimeException(String.format("There is no user in %s id ",userId)));
        return modelMapper.map(users,UserResponse.class);

    }

    public UserResponse update(Long userId, UserRequest request) {
        Users users = userRepository.findById(userId)
                .orElseThrow(()-> new RuntimeException(String.format("There is no user in %s id ",userId)));

        users = modelMapper.map(request,Users.class);
        users.setName(request.getName());
        users.setEmail(request.getEmail());
        users.setPassword(request.getPassword());

        userRepository.save(users);
        return modelMapper.map(users,UserResponse.class);
    }

    public void delete(Long userId) {
        userRepository.deleteById(userId);
    }

}
