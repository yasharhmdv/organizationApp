package com.example.task.service;

import com.example.task.dto.OrganizationRequest;
import com.example.task.dto.OrganizationResponse;
import com.example.task.dto.UserResponse;
import com.example.task.entity.Organization;
import com.example.task.entity.Users;
import com.example.task.repository.OrganizationRepository;
import com.example.task.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class OrganizationService {
    private final OrganizationRepository organizationRepository;
    private final UserRepository userRepository;
    private final ModelMapper modelMapper;

    public void create(OrganizationRequest request) {
        Organization organization = modelMapper.map(request,Organization.class);
        organizationRepository.save(organization);
    }

    public OrganizationResponse get(Long id) {
        Organization organization = organizationRepository.findById(id)
                .orElseThrow(()->new RuntimeException(String.format("There is no organization in id %s",id)));

        return modelMapper.map(organization,OrganizationResponse.class);
    }

}
